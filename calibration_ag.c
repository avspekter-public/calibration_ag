#include "calibration_ag.h"
int32_t get_cal_enum_e_t_type(int32_t cal_enum_name) {
    if (cal_enum_name >= START_CALIBRATION_SETT_ENUM && cal_enum_name <= END_CALIBRATION_SETT_ENUM) {
        return CAL_COMM_TYPE_SETTING;
    }

    if (cal_enum_name >= START_CALIBRATION_EVENT_ENUM && cal_enum_name <= END_CALIBRATION_EVENT_ENUM) {
        return CAL_COMM_TYPE_EVENT;
    }

    if (cal_enum_name >= START_CALIBRATION_RES_ENUM && cal_enum_name <= END_CALIBRATION_RES_ENUM) {
        return CAL_COMM_TYPE_RESULT;
    }

    return END_CALIBRATION_COMM_TYPE_ENUM;
}
